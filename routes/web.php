<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('posts');
});


Auth::routes();
Route::get('/', 'PostController@index')->name('home');
Route::get('posts', 'PostController@index')->name('posts');
Route::get('posts/create', 'PostController@create')->name('posts.create');
Route::get('posts/edit/{id}', 'PostController@edit')->name('posts.edit');
Route::post('posts/edit/{id}', 'PostController@update')->name('posts.update');

Route::get('comments/edit/{id}', 'CommentController@edit')->name('comments.edit');
Route::post('comments/edit/{id}', 'CommentController@update')->name('comments.update');


Route::post('posts', 'PostController@store')->name('posts.store');

Route::get('posts/{id}', 'PostController@show')->name('posts.show');
Route::delete('posts/{id}', 'PostController@destroy')->name('posts.destroy');
