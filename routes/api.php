<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('posts', 'PostController@apiStore')->name('api.posts.store');
Route::get('posts/{id}', 'PostController@apiShow')->name('api.comments.show');
Route::delete('posts/id', 'PostController@apiDestroy')->name('api.comments.destroy');
Route::patch('posts', 'PostController@apiPatch')->name('api.comments.edit');
