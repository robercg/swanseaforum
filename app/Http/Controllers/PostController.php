<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts =  Post::orderBy('id', 'desc')->paginate(10);
        return view('posts.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
        'content' => 'required|max:500',
        'title' => 'required|max:50',
      ]);
      $a = new Post;
      $pic = $request->file('picture');
      if(isset($pic)) {
        $extension = $pic->getClientOriginalExtension();
        Storage::disk('public')->put($pic->getFilename().'.'.$extension,  File::get($pic));
        $a->mime = $pic->getClientMimeType();
        $a->original_filename = $pic->getClientOriginalName();
        $a->filename = $pic->getFilename().'.'.$extension;
      }

      $a->content = $validatedData['content'];
      $a->admin_id = Auth::user()->id;
      $a->title = $validatedData['title'];
      $a->save();

      return view('posts.show', ['post' => $a]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apiStore(Request $request)
    {
      $validatedData = $request->validate([
        'content' => 'required|max:500',
        'post_id' => 'required|integer',
        'user_id' => 'required|integer',
      ]);
      $a = new Comment;
      $a->content = $validatedData['content'];
      $a->user_id = $validatedData['user_id'];
      $a->post_id = $validatedData['post_id'];
      $a->save();

      session()->flash('message','New comment added');
      return $a;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show', ['post' => $post]);

    }

    public function apiShow($id)
    {
        $comments = Comment::with('user')->where('post_id', '=', $id)->get();
        return $comments;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $post = Post::findOrFail($id);
      return view('posts.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
        'content' => 'required|max:500',
        'title' => 'required|max:50',
      ]);
      $post = Post::findOrFail($id);
      $post->content = $validatedData['content'];
      $post->title = $validatedData['title'];
      $post->save();

      return view('posts.show', ['post' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findorFail($id);
        $post -> delete();
        return redirect()->route('home');

    }

    public function apiDestroy(Request $request)
    {
      $validatedData = $request->validate([
        'id' => 'required',
      ]);
        $aid = $validatedData['id'];
        $comment = Comment::findorFail($aid);
        $comment -> delete();
    }

}
