<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profile;

class User extends Authenticatable
{
  protected $guarded = [];

    public function posts(){
      return $this->hasMany('App\Post');
    }

    public function comments(){
      return $this->hasMany('App\Comment');
    }

    public function profile(){
      return $this->hasOne('App\Profile');
    }

}
