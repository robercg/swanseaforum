<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
Use App\Tag;

class Post extends Model
{
  public function user(){
    return $this->belongsTo('App\User', 'admin_id');
  }

  public function comments(){
    return $this->hasMany('App\Comment');
  }

  public function tags(){
    return $this->belongsToMany('App\Tag');
  }
}
