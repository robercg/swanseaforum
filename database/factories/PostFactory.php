<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    return [
      'title' => $faker -> sentence(10),
      'content' => $faker -> sentence(20),
      'admin_id' => $faker -> randomElement(App\User::where('isAdmin', '=',1)->pluck('id')->toArray()),
    ];
});
