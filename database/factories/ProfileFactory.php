<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'location' => $faker -> city,
        'user_id' => $faker->unique()->numberBetween(1, App\User::count()),
    ];
});
