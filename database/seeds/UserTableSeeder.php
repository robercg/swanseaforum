<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = new User;
        $a->name = "Paco";
        $a->isAdmin = true;
        $a->email = "paco@gmail.com";
        $a->password="123";
        $a->save();

        factory(App\User::class,40)->create();
    }
}
