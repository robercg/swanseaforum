<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\Tag;
class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $t = new Tag;
      $t->name = "General";
      $t->save();
      $t->posts()->attach(1);
      $t->posts()->attach(5);
      $t->posts()->attach(39);


      $a = new Tag;
      $a->name = "Politics";
      $a->save();
      $a->posts()->attach(1);
      $a->posts()->attach(5);
      $a->posts()->attach(39);


      $b = new Tag;
      $b->name = "Sports";
      $b->save();
      $b->posts()->attach(1);
      $b->posts()->attach(40);
      $b->posts()->attach(5);
    }
}
