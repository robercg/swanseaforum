<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $a = new Post;
      $a->content = "Hello I am new here";
      $a->title = "Big hello";
      $a->admin_id = 1;
      $a->save();

      factory(App\Post::class,40)->create();

    }
}
