<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $a = new Comment;
      $a->content = "Hello I am a comment";
      $a->user_id = 1;
      $a->post_id = 1;
      $a->save();


      factory(App\Comment::class,40)->create();

    }
}
