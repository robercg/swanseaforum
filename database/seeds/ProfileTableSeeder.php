<?php

use Illuminate\Database\Seeder;
use App\Profile;
class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $a = new Profile;
      $a->location = "Algeciras";
      $a->user_id = 1;
      $a->save();

        factory(App\Profile::class,40)->create();
    }

}
