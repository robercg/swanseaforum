@extends('layouts.app')
@section('title', 'Swanseablog')
@section('title-navbar', 'Swanseablog')

@section('content')
<div style="padding-left:20px";>
<p><a href="{{ route('posts')}}">Back</a></p>
<h3>Thread: {{ $post -> title}}</h3></div></p></p>
  <ul>
    <blockquote class="blockquote">
     <p class="mb-0">  {{ $post -> content}}</p>
      <img src="{{url('uploads/'.$post->filename)}}" alt="{{$post->filename}}" height="200" width="200">

     @if(Auth::check() && (Auth::user()->isAdmin))
     <form method="POST" action="{{route('posts.destroy',['id' => $post ->id]) }}">
        @csrf
        @method('DELETE')
       <input style="text-align: center;border-radius:5px;" type="submit"value="DELETE" class="btn btn-danger btn-sm">
     </form>
     <form method="GET"
        action="{{route('posts.edit',['id' => $post ->id]) }}">
        @csrf
        @method('DELETE')
       <input style="text-align: center;border-radius:5px;" type="submit"value="EDIT" class="btn btn-primary btn-sm">
     </form>
     @endif
     <footer class="blockquote-footer">Posted by {{ $post -> user -> name }}</cite></footer>

     <footer class="blockquote-footer">Location: {{ $post -> user -> profile -> location }}</cite></footer>

     <footer class="blockquote-footer">Tags:
         @foreach ($post -> tags as $tag)
          {{ $tag -> name }}

         @endforeach </cite></footer>
    </blockquote>
  </ul>

  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <?php
    if(Auth::check()){
      $usernum = Auth::user()->id;
    }
   use App\User ?>

 <div id ="root" style="padding:20px">
 <h4>Comments</h4>
   <ul class="list-group">
     <li class="list-group-item" v-for="comment in comments">
       @{{comment.content}}
        <footer class="blockquote-footer">Comment by @{{comment.user.name}}</cite></footer>
        @if(Auth::check())
        <div v-if="({{Auth::check()}} && comment.user_id === {{$usernum}}) || {{(Auth::user()->isAdmin)}} ">
       <a class="btn btn-default" :href="'/comments/edit/' + comment.id">Edit</a>
       <a class="btn btn-default" v-on:click=" deleteComment(comment.id)">Delete</a>
       @endif
     </li>
   </ul>
  @if(Auth::check())
<p></p>
        <h4>Add Comment</h4>
       <form action="" @submit.prevent="newComment()">
         <div class="input-group">
           <textarea name="comment.content" v-model="comment.content" ref="textarea"  class="form-control"></textarea>
         </div>
           <div class="input-group">
             <button type="submit" class="btn btn-primary" v-show="!edit">Add Comment</button>
           </div>
       </form>

     </div>
</div>
@endif

<script>
      var app = new Vue({
        el: "#root",
        data: {
          comments:[],
          comment: {
            content: '',
            id: '',
          }
        },
        mounted(){
          axios.get("{{ route('api.comments.show', ['id' => $post->id])}}")
          .then(response => {
            console.log(response.data);
            this.comments = response.data;
          })
          .catch(response => {
            console.log(23);
            console.log(response);
            console.log(24);
          })
        },
        methods: {
                @if(Auth::check())
                getComments(){
                  axios.get("{{ route('api.comments.show', ['id' => $post->id])}}")
                  .then(response => {
                    console.log(response.data);
                    this.comments = response.data;
                  })
                  .catch(response => {
                    console.log(23);
                    console.log(response);
                    console.log(24);
                  })
                },

                deleteComment (comment_id){
                  axios.delete("{{route('api.comments.destroy')}}", {params: {'id': comment_id}})
                  .then(response =>{
                    this.comment.content = '';
                    this.getComments();
                })
                  .catch(response =>{
                    console.log(comment_id);
                    console.log(response);
                  })
                },

                  newComment (){
                     axios.post("{{route('api.posts.store')}}", {
                       "content": this.comment.content,
                       "post_id": {{ $post -> id}},
                       "user_id": {{ Auth::user()->id }},
                     })
                     .then(response =>{
                  response.data.user = {{User::where('id', '=', 'Auth::user()->id')->get()}}
                       console.log(25);
                       console.log(this.content);
                       console.log(response.data);
                       this.comments.push(response.data);
                       this.content = '';
                       this.post = '';
                     })
                     .catch(response =>{
                       console.log(response);
                     })
                   }


                  @endif
                }
              });
        </script>

@endsection
