@extends('layouts.app')

@section('title', 'Posts')
@section('title-navbar', 'Swanseablog')

@section('content')
  <div class ="text-center">
    <h3>Current threads </h3>
  </div>
  <ul class="list-group">
    <div class="container">
      @if(Auth::check() && (Auth::user()->isAdmin))
        <a href="{{ route('posts.create') }}"><input style="text-align: center;border-radius:5px;"  type="submit" value="NEW THREAD" class="btn btn-info btn-sm"></a>
      @endif
      @foreach ($posts as $post)
      <a href="{{ route('posts.show', ['id' => $post->id])}}"
        class="list-group-item list-group-item-action" >{{ $post -> title }}</a>
      @endforeach
    </div>
  <p></p><ul class="pagination justify-content-center"> {{$posts->links()}}</ul>
  </ul>
@endsection
