@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit comment') }}</div>

                <div class="card-body">
                      <form method="POST" action="{{ route('comments.update',['id' => $comment ->id]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('Content') }}</label>

                            <div class="col-md-6">
                                <textarea  rows="5" id="content" type="content" class="form-control input-lg" name="content" value="{{ $comment -> content }}"><?=$comment->content?></textarea>

                                @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" value="Submit">
                                    {{ __('Post') }}</a>
                                </button>
                                <button type="submit" class="btn btn-secondary" >
                                  <a href="{{ route('posts')}}">
                                    {{ __('Cancel') }}
                                </button></a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
